//
//  Bdd.swift
//
//  Created by Venkat Peri on 6/13/14.
//  Copyright (c) 2014 CameraSpice Inc. All rights reserved.
//

import XCTest

protocol FeatureBlock {
  typealias ReturnType
  var type : String { get }
  var desc : String { get }
  var block : () -> ReturnType { get }
  var message : String { get }
  var failed : Bool { get }
  func execute()
}

class BaseFeatureBlock {
  var type: String
  var desc : String
  var message : String
  var failed = false
  
  init(t: String, d: String ) {
    type = t
    desc = d
    message = "\(type) \"\(desc)\""
  }
  
  func execute() {}
}

class VoidBlock : BaseFeatureBlock, FeatureBlock {
  typealias ReturnType = Void
  var block : () -> ReturnType
  
  init(t: String, d: String, b: ()->ReturnType ) {
    block = b
    super.init(t: t,d: d)
  }
  
  override func execute() {
    block()
  }
}

class BoolBlock : BaseFeatureBlock, FeatureBlock {
  typealias ReturnType = Bool
  var block : () -> ReturnType
  
  init(t: String, d: String, b: ()->ReturnType ) {
    block = b
    super.init(t: t,d: d)
  }
  
  override func execute() {
    failed = !block()
    message += failed ? "  [FAIL]" : "  [PASS]"
  }
}

class When : VoidBlock {
init(d: String, b: ()->()){ super.init(t: "when", d: d, b)  }
}

class Then : BoolBlock {
  init(d: String, b: ()->Bool ){ super.init(t: "then", d: d, b)  }
}

class Given : VoidBlock {
  init(d: String, b: ()->()){ super.init(t: "given", d: d, b)  }
}

class Expect : BoolBlock {
  init(d: String, b: ()->Bool ){ super.init(t: "expect", d: d, b)  }
}

class Feature : BaseFeatureBlock, FeatureBlock {
  typealias ReturnType = Void
  typealias Block = () -> ReturnType
  
  var block: Block
  var parent: Feature? = nil
  var setup: Block?
  var cleanup: Block?
  var given: Given?
  var when: When?
  var then: Then?
  var expect: Expect?
  var beforeFeature : Block?
  var afterFeature : Block?
  var children = Feature[]()
  
  init(name: String, block: Feature.Block, parent: Feature? = nil) {
    self.parent = parent
    self.block = block
    super.init(t: "describe", d: name)
  }
  
  var closesBeforeFeature : Block? {
  return beforeFeature ? beforeFeature! : (parent ? parent!.closesBeforeFeature : nil)
  }
  
  var closestAfterFeature : Block? {
  return afterFeature ? afterFeature! : (parent ? parent!.closestAfterFeature : nil)
  }
  
  func visit(visitor: (FeatureBlock, Int) -> (), level : Int = 0) {
    if let before = closesBeforeFeature { before() }
    
    visitor(self, level)
    
    if let g = given { visitor(g, level+1) }
    if let w = when {
      visitor(w, level+1)
      assert(then, "when block needs a then block")
      if let t = then { visitor(t, level+1) }
    }
    if let e = expect { visitor(e, level+1) }
    
    for c in children { c.visit(visitor, level: level + 1) }
    
    if let after = closestAfterFeature { after() }
  }
}

protocol Reporter {
  func report( Feature, BddSpec )
}

class ConsoleReporter : Reporter{
  func report(feature: Feature, _ spec: BddSpec) {
    feature.visit { (x: FeatureBlock, level: Int) -> () in
      let leader = repeat(" ", level*2)
      println("\(leader)\(x.message)")
      if x.failed {
        spec.recordFailureWithDescription(x.message, inFile: __FILE__, atLine: __LINE__, expected: true)
      }
    }
  }
}

class BddSpec: XCTestCase {
  
  var feature = Feature(name: "top", block: {()->() in})
  var reporters = [ ConsoleReporter() ]
  
  func given(desc: String, block: ()->() ) { feature.given = Given(d: desc, block) }
  func when(desc: String, block: ()->() ){ feature.when = When(d: desc, block) }
  func then(desc: String, block: ()-> Bool){ feature.then = Then(d: desc, block) }
  func expect(desc: String, block: ()->Bool ){ feature.expect = Expect(d: desc, block) }
  func beforeFeature( block: ()->() ){ feature.beforeFeature = block }
  func afterFeature( block: ()->() ){ feature.afterFeature = block }
  
  func describe( name : String, block : Feature.Block) {
    var f = Feature(name: name, block: block, parent: feature)
    feature.children += f
    
    //become the child to configure it via its block
    feature = f
    feature.block()
    feature = feature.parent!
  }
  
  func specify() {}
  
  func testRunner() {
    specify()
    
    //run the tests
    feature.visit { $0.0.execute() }
    
    //report
    for r in reporters { r.report(feature, self) }
  }
}


func repeat( item: String, n: Int) -> String {
  var s = ""
  for var i:Int=0; i<n; i++ { s += item }
  return s
}


