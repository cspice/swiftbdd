//
//  StackTest.swift
//  Masala
//
//  Created by Venkat Peri on 6/13/14.
//  Copyright (c) 2014 CameraSpice Inc. All rights reserved.
//

import Foundation

class Stack<T> {
  var data = T[]()
  var count : Int { return data.count }
  func push( x : T ) { data.insert(x, atIndex: 0) }
  func pop() -> T? { return count > 0 ? data.removeAtIndex(0) : nil  }
}


class Test : BddSpec {
  
  var stack = Stack<Int>()
  var value = 0
  
  override func specify() {
    
    describe( "a stack" ) {
      self.beforeFeature {
        self.stack = Stack()
      }
      
      self.given("a new stack") {}
      self.expect("it is empty") {
        self.stack.count == 0
      }
      
      self.describe("pushing an element on the stack") {
        self.given("a stack") {
          self.stack.push(1)
          self.stack.push(2)
          self.value = self.stack.count
        }
        
        self.when("an element is pushed") {
          self.stack.push(100)
        }
        
        self.then("the stack size increases by one") {
          self.stack.count == self.value + 1
        }
      }
      
      self.describe("popping an element on the stack") {
        self.given("a non-empty stack") {
          self.stack.push(1)
          self.stack.push(2)
          self.stack.push(102)
        }
        
        self.when("an element is popped off the stack") {
          self.value = self.stack.pop()!
        }
        
        self.then("it is the element most recently pushed onto the stack") {
          self.value == 102
        }
      }
      
      self.describe("popping an element off the stack") {
        self.given("an empty stack") { }
        
        self.expect("popping the stack returns NIL") {
          self.stack.pop() == nil
        }
      }
    }
  }
}
